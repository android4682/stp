<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Article;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketController extends AbstractController
{
  /**
   * @Route("/")
   * @Method({"GET"})
   */
  public function index()
  {
    $catagories = $this->getDoctrine()->getRepository(Category::class)->findAll();
    // return new Response("<html><body>Works</body></html>");
    return $this->render("index.html.twig", array('categories' => $catagories));
  }
  
  /**
   * @Route("/category/{id}")
   * @Method({"GET"})
   */
  public function category($id)
  {
    $catagory = $this->getDoctrine()->getRepository(Category::class)->find($id);
    $articles = $this->getDoctrine()->getRepository(Article::class)->findBy(array('category' => $id));
    return $this->render("category.html.twig", array('category' => $catagory, 'articles' => $articles));
  }
  
  /**
   * @Route("/all_articles")
   * @Method({"GET"})
   */
  public function all_article()
  {
    $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
    return $this->render("all_articles.html.twig", array('articles' => $articles));
  }
  
  /**
   * @Route("/article/{id}", name="article")
   * @Method({"GET"})
   */
  public function article($id)
  {
    $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
    return $this->render("article.html.twig", array('article' => $article));
  }

  /**
   * @Route("/test")
   * @Method({"GET"})
   */
  public function test()
  {
    $articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
    $name = $articles[0]->getCategory()->getName();
    return new Response("Name: $name");
  }
}