<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Article;
use App\Form\NewArticleType;
use App\Repository\ArticleRepository;

class AdminPanelController extends AbstractController
{
    /**
     * @Route("/admin", name="admin", methods={"GET"})
     */
    public function index(): Response
    {
        if (!$this->getUser()) return $this->redirectToRoute("login");
        return $this->render('admin_panel/index.html.twig');
    }

    /**
     * @Route("/admin/manage_articles", name="manage_articles", methods={"GET"})
     */
    public function manage_articles(): Response
    {
        if (!$this->getUser()) return $this->redirectToRoute("login");
        $all_articles = $this->getDoctrine()->getRepository(Article::class)->findAll();
        return $this->render('admin_panel/manage_articles.html.twig', [
            'all_articles' => $all_articles
        ]);
    }

    /**
     * @Route("/admin/new_article", methods={"GET", "POST"})
     */
    public function new_article(Request $request): Response
    {
        // Is user autheticated?
        $user = $this->getUser();
        if (!$user) return $this->redirectToRoute("login");
        $article = new Article();
        $form = $this->createForm(NewArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($user);
            $article->setPublished_on(new \DateTime());
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            return $this->redirectToRoute('article', ["id" => $article->getId()]);
        }

        return $this->render('admin_panel/new_article.html.twig', [
            'form' => $form->createView(),
            'edit_mode' => false
        ]);
    }

    /**
     * @Route("/admin/edit_article/{id}", methods={"GET", "POST"})
     */
    public function edit_article(Request $request, $id): Response
    {
        $user = $this->getUser();
        // Is user autheticated?
        if (!$user) return $this->redirectToRoute("login");
        if (empty($id)) return $this->redirectToRoute("admin");
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        if (empty($article)) return $this->redirectToRoute("admin");
        $form = $this->createForm(NewArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
            return $this->redirectToRoute('article', ["id" => $article->getId()]);
        }

        return $this->render('admin_panel/new_article.html.twig', [
            'form' => $form->createView(),
            'edit_mode' => true
        ]);
    }

    /**
     * @Route("/admin/delete_article/{id}", methods={"DELETE"})
     */
    public function delete_article(Request $request, $id): Response
    {
        $user = $this->getUser();
        // Is user autheticated?
        if (!$user) return new Response('not_authenticated', 401);
        if (empty($id)) return new Response('missing_parameter_id', 400);
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        if (empty($article)) return new Response('item_not_found', 404);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        return new Response();
    }
}
