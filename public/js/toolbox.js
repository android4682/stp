/**
 * My famous (not really) isEmpty function. It checks if a given variable is empty or not
 * @param {*} variable Anything you want to know if it's empty or not
 * @returns {Boolean} Returns true on empty and false on not empty
 */
 function isEmpty(variable) {
  if (variable == null || variable == undefined || Number.isNaN(variable)) return true
  if (typeof variable == "string" && variable == "") return true
  else if (typeof variable == "object") {
    if (Array.isArray(variable)) {
      if (variable.length == 0) return true
    } else if (Object.keys(variable).length == 0) return true
  }
  return false
}

function findDataFromParent(element, dataName) {
  let snowflake = $(element).attr("data-" + dataName)
  if (!isEmpty(snowflake)) return snowflake
  else if (element.localName == "body") return null
  do {
    element = element.parentElement
    snowflake = $(element).attr("data-" + dataName)
    if (!isEmpty(snowflake)) return snowflake
  } while (element !== null && element.localName != "body")
  return null
}